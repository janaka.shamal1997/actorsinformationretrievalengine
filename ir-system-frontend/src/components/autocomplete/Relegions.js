export const relegionsList = [
  {
    label: "බුද්ධාගම",
  },
  {
    label: "හින්දු ආගම",
  },
  {
    label: "ඉස්ලාමය",
  },
  {
    label: "ක්‍රිස්තියානි",
  },
  {
    label: "කතෝලික ධර්මය",
  },
  {
    label: "යුදෙව් ආගම",
  },
  {
    label: "අදේවවාදය",
  },
  {
    label: "ක්රමවේදය",
  },
  {
    label: "එපිස්කෝපල්",
  },
];
