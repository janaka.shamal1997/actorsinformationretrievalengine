import * as React from "react";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import { Typography } from "@mui/material";

export default function SingleResult({ result }) {
  return (
    <Box
      sx={{
        display: "flex",
        flexWrap: "wrap",
        "& > :not(style)": {
          m: 1,
        },
      }}
    >
      <Paper 
              elevation={3} 
              style={{ 
              justifyContent: 'center',
              backgroundColor: "transparent", 
              color: "white",
              background: 'linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)',
              border: 0,
              borderRadius: 0,
              boxShadow: '0 3px 5px 2px rgba(33, 203, 243, .3)',
              color: 'white',
              padding: '10px 30px',
              marginLeft:'100px'
              }}>
        <Typography variant="h6">{result}</Typography>
      </Paper>
    </Box>
  );
}
