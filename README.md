# Actors Information Retrieval Engine In Sinhala

This is a simple project built using React frontend and a Python Flask backend server which design for search actors using Elastic search.

![Search Engine](images/actors_search_engine.png)
## Getting started
### Setting Up Elasticsearch
1.Download, install and run [Elasticsearch](https://www.elastic.co/downloads/elasticsearch).
2.Install ICU Analysis plugin.
3.Install Kibana for query operations.

### Setting Up the Index
> Alternatively, you can restore the index from the provided elasticsearch snapshot in the `es_snapshots/` folder [See more on snapshot and restore](https://www.elastic.co/guide/en/elasticsearch/reference/current/snapshot-restore.html)

1. Create an index named `actors` in the Elasticsearch and execute below queries.
( You can use Kibana to create index and run queries easily )
```

PUT /actors
{
  "settings": {
    "index": {
      "analysis": {
        "analyzer": {
          "sinhalaAnalyzer": {
            "type": "custom",
            "tokenizer": "icu_tokenizer",
            "filter": ["edgeNgram"],
            "char_filter": ["dotFilter"]
          }
        },
        "filter": {
          "edgeNgram": {
            "type": "edge_ngram",
            "min_gram": 2,
            "max_gram": 50,
            "side": "front"
          }
        },
        "char_filter": {
          "dotFilter": {
            "type": "mapping",
            "mappings": ". => \\u0020"
          }
        }
      }
    }
  }
}
```
```

PUT actors/_mappings/
{
  "properties": {
    "නම": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      },
      "analyzer": "sinhalaAnalyzer",
      "search_analyzer": "standard"
    },
    "පෞද්ගලික ජීවිතය": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      },
      "analyzer": "sinhalaAnalyzer",
      "search_analyzer": "standard"
    },
    "වෘත්තිය": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      },
      "analyzer": "sinhalaAnalyzer",
      "search_analyzer": "standard"
    },
    "උපන් දිනය": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      },
      "analyzer": "sinhalaAnalyzer",
      "search_analyzer": "standard"
    },
    "උපන් ස්ථානය": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      },
      "analyzer": "sinhalaAnalyzer",
      "search_analyzer": "standard"
    },
        "අධ්‍යාපනය": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      },
      "analyzer": "sinhalaAnalyzer",
      "search_analyzer": "standard"
    },
    "ජාතිය": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      },
      "analyzer": "sinhalaAnalyzer",
      "search_analyzer": "standard"
    },
    "ආගම": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      },
      "analyzer": "sinhalaAnalyzer",
      "search_analyzer": "standard"
    }
  }
}

```
2. To add documents to the created index using the [Bulk API](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-bulk.html) run bulk.py along with actors.json file

> You may use Kibana/POSTMAN  or any other option for query operations.

### Setting Up the Python Server
1. Install python and pip version 3
2. Install required python packages by running the following command in the project home directory. `$ pip install -r requirements.txt`
3. Download and setup [SinLing](https://github.com/nlpc-uom/Sinling). You may have to append project path to your path environment variable.
4. Configure the index name and Elasticsearch host:port details in `/server/app.py` file.
```
index_name = 'actors'
es = Elasticsearch('localhost', port=9200)
```

### Setting Up React Front-end
1. Download and install required node packages by running `npm install` in the `/ir-system-frontend` directory, then run.

## Running the Project
1. Run the Elasticsearch instance.
2. To Run python backend server by executing `python main.py` in the `/ir-system-server` directory.
3. Run the React web app by executing `npm start` command in the `/ir-system-frontend` directory.

## Basic Usage Examples
### Basic Search
* Search by the birth location.
![search by birth location example](images/query1.PNG)

* Search by the birth year/month.
![search by birth year example](images/query2.PNG)

* Search by a phrase.
![search by a phrase example](images/query3.PNG)

* Search by the actor's name.
![search by name example](images/Query4.PNG)

* Search for the school/ nationality/ religion of a given actor.
![search for the school example](images/Query5.PNG)

### Advance Search
* Advance search with more filters.
![search by phrase example](images/advance_search.PNG)

## Additional Details
The project utilizes the below query types in Elasticsearch.
* [Multi-match query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-multi-match-query.html) with certain fields boosted
* [Boolean query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-bool-query.html)

Aditionally, below query options were also used.
* [Terms Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-terms-aggregation.html)
* [Sort](https://www.elastic.co/guide/en/elasticsearch/reference/6.8/search-request-sort.html)

This project uses the `Sinhala Tokenizer` from [SinLing](https://github.com/nlpc-uom/Sinling), a language processing tool for Sinhala language.

Also, the project uses a `Sinhala Stemmer` from 'https://github.com/e11379dana/SinhalaStemming'.

English to Sinhala translations are done using the [translate](https://pypi.org/project/translate/) python package.
